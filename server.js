var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var path = require('path');

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2dbp/collections/movimientos?apiKey=qJOr0akFh8-E5gBFyD4De96zfwBji7mN";

var clienteMlab = requestjson.createClient(urlmovimientosMlab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function (req, res){
  //res.send("Hola mundo");
  res.sendFile(path.join(__dirname,'index.html'));
})

app.get("/movimientos",function (req, res){
  clienteMlab.get ('', function(err, resM, body) {
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.get("/clientes/:idcliente",function (req, res){
  //var mivar="Aqui tiene al cliente nùmero: " + req.params.idcliente;
  //res.send(mivar);
  res.send("Aqui tiene al cliente nùmero: "+ req.params.idcliente);
  //res.status(200).send("Aqui tiene al cliente nùmero: "+ req.params.idcliente);
})


app.post("/",function (req,res){
  res.send("Hemos recibido su peticiòn actualizada");
})

/*

app.post("/movimientos",function (req,res){
  var jsonNuevoCliente = {
    "nombre":"Yoyo",
    "apellido":"Node",
    "idcliente":0987
  };
  clienteMlab.post ('',jsonNuevoCliente, function(err, resM, body) {
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})
*/
app.post("/movimientos",function (req,res){
  clienteMlab.post ('',req.body, function(err, resM, body) {
  res.send(body);
  })
})

app.put("/",function (req,res){
  res.send("Hemos recibido su peticiòn PUT");
})

app.delete("/",function (req,res){
  res.send("Hemos recibido su peticiòn DELETE");
})
